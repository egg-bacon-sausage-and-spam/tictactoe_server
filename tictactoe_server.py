#!/usr/bin/python3

import asyncio
from configparser import ConfigParser

from ttt.session import session

async def handle_client(reader, writer):

    s = session(reader, writer)

    while True:
        if writer.is_closing():
            break
        if s.done():
            await s.write_pretty('EXITING')
            break
        await s.next()

    if writer.is_closing() is False:
        await writer.drain()

    print("Closing the connection")
    writer.close()

async def main():
    config = ConfigParser()

    # provide defaults even if they're not set in the config file
    config.read_dict({
        'server': {
            'listen_addr': '127.0.0.1',
            'listen_port': '9669'
        }
    })

    config.read('server.ini')

    listen_addr = config['server']['listen_addr']
    listen_port = config['server']['listen_port']

    server = await asyncio.start_server(
        handle_client, listen_addr, listen_port)

    addr = server.sockets[0].getsockname()
    print(f'Serving on {addr}')

    async with server:
        await server.serve_forever()

asyncio.run(main())
