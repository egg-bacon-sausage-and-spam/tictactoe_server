class match:
    def __init__( self ):
        self.plays = []
        self.cheat_reason = []
        self.finished = False

    def check_valid_moves( self ):
        """Check that the current matrix has just one more element than the last"""
        if len( self.plays ) < 2:
            return True
        num_x = 0
        num_o = 0
        diff = 0
        m = self.plays[-2].matrix
        n = self.plays[-1].matrix


        for i in [0, 1, 2]:
            # count the x's and o's
            num_x += n[i].count(1)
            num_o += n[i].count(2)
            for j in [0, 1, 2]:
                if n[i][j] != m[i][j]:
                    diff += 1

                    if self.plays[-2].matrix[i][j] != 0:
                        # This field was changed!
                        self.cheat_reason.append( 'A non-empty field was changed.' )
                        break

        if diff == 1:
            if ( num_x - num_o ) not in [ 0, 1 ]:
                self.cheat_reason.append("You didn't play oughts!")

        if diff == 1 and self.cheat_reason == []:
            return True
        else:
            if diff != 1:
                self.cheat_reason.append( f' { diff } fields were changed, not 1.' )
            return False

    def free_fields( self ):
        free_fields = []

        for i in [0, 1, 2]:
            for j in [0, 1, 2]:
                if self.plays[-1].matrix[i][j] == 0:
                    free_fields.append( [i, j] )

        if len(free_fields) == 0:
            self.finished = True
        return free_fields

    def append( self, play ):
        self.plays.append( play )
        valid = self.check_valid_moves()
        return valid

    def return_winner ( self ):
        if len(self.plays) == 0:
            return False
        else:
            return self.plays[-1].winner
