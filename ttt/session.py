import asyncio
import copy
import logging, sys
from random import randint
from ttt.match import match
from ttt.play import play

class session:
    def __init__( self, reader, writer ):

        (self.ip, self.port) = reader._transport.get_extra_info("peername")
        logging.basicConfig(
            format='%(levelname)s:%(message)s',
            stream=sys.stderr,
            level=logging.DEBUG
        )

        self.__reader = reader
        self.__writer = writer

        logging.info(
            f'Initiating session with client: { self.__reader._transport.get_extra_info("peername") }'
        )

        self.player_name = None
        self.my_turn = True
        self.finished = False
        self.match = match()
        self.strikes = 0
        self.sleeptime = 0.5

    async def __sleep( self ):
        sleeptime = self.sleeptime
        await asyncio.sleep( sleeptime )


    def done ( self ):
        """check if match is finished, and return true"""
        if self.finished or self.match.finished:
            return True
        else:
            return False

    def get_moves( self ):
        """return number of moves played in this match"""
        return len ( self.match.plays )

    async def read_move( self ):
        while True:
            line = await self.read_pretty()

            if self.strikes >= 3:
                await self.write_pretty('TOO MANY INVALID INPUTS. GIVING UP.')
                self.finished = True
                break

            if line == 'HELP':
                await self.__help()
            elif line == 'BEGIN MOVE':
                play_lines = []
                for i in range(5):
                    play_lines.append( await self.read_pretty() )

                your_play = play(field=play_lines)
                if your_play.invalid:
                    self.finished = True
                    await self.write_pretty('ERROR: INVALID MOVE')
                    break
                parse_status = self.match.append( your_play )

                last_line = await self.read_pretty()
                if last_line == 'END MOVE' and parse_status == True:
                    logging.debug( "Successfully parsed move" )
                else:
                    await self.write_pretty('ERROR: INVALID MOVE')
                    await self.write_pretty( self.match.cheat_reason )
                    self.finished = True
                break
            else:
                self.strikes += 1
                await self.write_pretty('UNKNOWN MOVE')
                await self.__help()

    async def play_move( self ):
        await self.write_pretty('BEGIN MOVE')

        # This is the first move of the match
        if self.get_moves() == 0:
            my_matrix = [
                [0, 0, 0],
                [0, 0, 0],
                [0, 0, 0]
            ]
            my_matrix[ randint(0, 2) ][ randint(0, 2) ] += 1

            current_play = play( matrix = my_matrix )
            self.match.append( current_play )
            await self.write_pretty( current_play.field )

        # This is a subsequent move of the match
        else:
            # copy the last matrix, which was verified
            my_move = copy.deepcopy( self.match.plays[-1].matrix )

            if len(self.match.plays[-1].best_next_plays) > 0:
                (x, y) = self.match.plays[-1].best_next_plays[0]
                logging.debug( f"Responding to threat at { x }, { y }" )
                my_move[x][y] = 1
            else:
                while True:
                    x = randint( 0, 2 )
                    y = randint( 0, 2 )
                    if my_move[x][y] == 0:
                        my_move[x][y] = 1
                        logging.debug( f"Playing random at { x }, { y }" )
                        break

            my_play = play ( matrix = my_move )

            self.match.append( my_play )
            await self.write_pretty( my_play.field )


        await self.write_pretty('END MOVE')

    async def ask_for_name( self ):

        await self.write_pretty( [
            "GREETINGS STRANGER.",
            "WHAT IS YOUR NAME?",
        ] )
        name = await self.read_pretty()

        logging.debug( f"Player name is: { name }" )

        self.player_name = name

        await self.write_pretty( [
            f"HELLO { self.player_name.upper() }.",
            "DO YOU WANT TO PLAY A GAME?",
            "TYPE HELP FOR HELP.",
            "I GOT BETTER AT PLAYING TIC TAC TOE."
        ] )

    async def read_pretty(self):
        read_input = ( await self.__reader.readline() ).decode().rstrip('\n')
        logging.debug( read_input )
        return read_input

    async def write_pretty(self, stuff_to_write):
        if self.__writer.is_closing() is True:
            return

        if type(stuff_to_write) == list:

            for string in stuff_to_write:
                if self.__writer.is_closing() is True:
                    return None
                await self.__sleep()
                logging.debug( string )
                self.__writer.write( (string+'\n').encode('utf-8') )

        elif type(stuff_to_write) == str:

            await self.__sleep()
            logging.debug( stuff_to_write )
            self.__writer.write( (stuff_to_write+'\n').encode('utf-8') )

    async def __help( self ):
        await self.write_pretty( [
            "Start your move by typing 'BEGIN MOVE'",
            "Then followed by 5 lines showing the tic tac toe field.",
            "Then ending the move with 'END MOVE'.",
            "An empty field must stay a space.",
            "My fields are marked with an x or an X.",
            "You can place an o or O on a free field.",
            "Here is an example field after the 4th move:",
            "---8<------8<------8<------8<---",
            "BEGIN MOVE",
            "O| | ",
            "-+-+-",
            " |o|x",
            "-+-+-",
            "X| | ",
            "END MOVE",
            "---8<------8<------8<------8<---"
        ] )

    async def next(self):
        """play next move, by either playing it, or reading it from the opponent"""

        if self.player_name == None:
            await self.ask_for_name()

        await self.write_pretty( f'MOVE #{self.get_moves()+1 } ({"your" if self.get_moves() % 2 else "my"} move)' )
        if self.my_turn == True:
            await self.play_move()
            self.my_turn = False
        else:
            await self.read_move()
            self.my_turn = True

        if len( self.match.cheat_reason ) > 0:
            return None

        if self.match.return_winner():
            self.finished = True
            if self.match.return_winner() == 1:
                await self.write_pretty('Oh noes! I won!')
            else:
                await self.write_pretty( f'Congrats! You "won"!' )
            return None

        if len( self.match.free_fields() ) == 0:
            self.finished = True
            await self.write_pretty( "It's a tie! See you next time!" )


        if self.get_moves() >= 9:
            self.finished = True
