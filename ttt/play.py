import logging
import re

class play:

    def __field2matrix( self, ttt_in: list, /) -> list:
        """Parse 5 lines of tic tac toe, returning a 3x3 matrix of the result

        The input should look like this:
         |O|
        -+-+-
         |x|
        -+-+-
        x| |X

        Valid values are x, X, o, O or ' ' for the fields.

        The above tic tac toe field will return following matrix:
        [[0, 2, 0], [0, 1, 0], [1, 0, 1]]
        """

        # regex to check the value fields
        re_check_line = re.compile('^[xXoO ]\|[xXoO ]\|[xXoO ]$')

        ## various checks about the tic tac toe field

        # must be 5 lines
        if len(ttt_in) != 5:
            self.invalid = True
            return None

        for i in range(5):

            # every line must be 5 chars long
            if len(ttt_in[i]) != 5:
                self.invalid = True
                return None

            # 2nd and 4th line must be a terminator
            if i in [1, 3]:
                if ttt_in[i] != '-+-+-':
                    self.invalid = True

            # 1st, 3rd and last line must look like regex
            if i in [0, 2, 4]:
                if not re_check_line.match( ttt_in[i] ):
                    self.invalid = True

        # use an 3x3 list of lists to represent the data
        ttt_out = []

        for i in [0, 2, 4]:
            ttt_line = []
            for j in [0, 2, 4]:
                if ttt_in[i][j] == ' ':
                    ttt_line.append(0)
                elif ttt_in[i][j] in ['x', 'X']:
                    ttt_line.append(1)
                else:
                    ttt_line.append(2)
            ttt_out.append(ttt_line)

        # return list of lists
        self.matrix = ttt_out

    def __matrix2field( self ):

        self.field = []

        for i in range(3):

            list = []

            for j in range(3):
                if self.matrix[i][j] == 0:
                    list.append(' ')
                elif self.matrix[i][j] == 1:
                    list.append('x')
                else:
                    list.append('o')

            self.field.append( '|'.join(list) )

            if i in [0, 1]:
                self.field.append( '-+-+-' )

    def __checkwin( self ):
        """Check if there are three X or O's in a line that show a win"""

        m = self.matrix

        # check for both X and O
        for sign in [1, 2]:
            if self.won == True:
                break
            for i in range(3):
                #check rows
                if m[i][0] == m[i][1] == m[i][2] == sign:
                    self.won = True
                    self.winner = sign
                    break
                # check columns
                elif m[0][i] == m[1][i] == m[2][i] == sign:
                    self.won = True
                    self.winner = sign
                    break

            # check diagonals
            if m[0][0] == m[1][1] == m[2][2] == sign:
                self.won = True
                self.winner = sign
                break
            elif m[0][2] == m[1][1] == m[2][0] == sign:
                self.won = True
                self.winner = sign
                break

    def __gen_lines( self ):
        """Generate a list of list of duplets, giving back the coordinates of all combinations of three fields"""
        if self.__all_lines is None:
            self.__all_lines = []
            for row in [ [ (x, y) for x in range(3) ] for y in range(3) ]:
                self.__all_lines.append(row)

            for column in [ [ (x, y) for y in range(3) ] for x in range(3) ]:
                self.__all_lines.append(column)

            # append diagonals
            self.__all_lines.append( [ (0,0), (1,1), (2,2) ] )
            self.__all_lines.append( [ (2,0), (1,1), (0,2) ] )

        return self.__all_lines

    def __find_threat( self ):
        m = self.matrix
        self.best_next_plays = []
        """find the x, y coordinates in the matrix that is missing for three in a row"""
        for sign in [1, 2]:
            for line in self.__gen_lines():
                assert len( line ) == 3
                if [ m[x][y] for x, y in line ].count( sign ) == 2:
                    # find out which line is has the empty field
                    for (x, y) in line:
                        if self.matrix[x][y] == 0:
                            self.best_next_plays.append( [x, y] )
        logging.debug( f'Found best plays: { self.best_next_plays }' )


    def __init__( self, matrix=None, field=None ):

        self.__all_lines = None
        self.best_next_plays = None
        self.invalid = False
        self.won = False
        self.winner = None


        if matrix is None:
            self.field = field
            self.__field2matrix( field )
        elif field is None:
            self.matrix = matrix
            self.__matrix2field()

        if self.invalid is True:
            # Parsing failed somewhere
            return None

        self.__checkwin()
        self.__find_threat()
